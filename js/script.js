class Card {
    constructor(){
        this.container = document.querySelector('.centrCard')
        this.modal = document.querySelector('.modal')
        this.searchInput = document.querySelector('input')
        this.bindListener()
        this.renderCards()

    }

    bindListener(){
        this.searchInput.oninput = (e) => {
            const value = e.target.value
            const filteredItems = items.filter(item => {
                return item.name.includes(value)
            })
            this.renderCards(filteredItems)
        }
    }

    _createCard(data){
        const card = document.createElement('div')
        card.onclick = (event) => {
            this.renderModal(data)
        }
        card.innerHTML = `
        <div class="card">
        <div class="zone">
            <img class="photo" src="img/${data.imgUrl}" alt="">
            <img class="svg" src="img/icons/like_empty.svg" alt="">
        </div>
        <div class='text'>
            <h4>${data.name}</h4>
            <p class="stock">
                <img src="img/icons/check.svg" alt="">
                ${data.orderInfo.inStock} left in stock
            </p>
            <p class="prise">prise: ${data.price}$</p>
        </div>
        <div class="button">
            <button class="
            ">add to cart</button>
        </div>
        <div class="footerCard">
            <div class="left">
                <img src="img/icons/like_empty.svg" alt="">
            </div>
            <div class="center">
                <p>${data.orderInfo.reviews}% Positive reviews</p>
                <p>Adove avarage</p>
            </div>
            <div class="right">
                <p>${Math.floor(Math.random() * (10000 - 0) + 0)} orders</p>
            </div>
        </div>
    </div>`
        return card
    }

    renderCards (arr=items){
        this.container.innerHTML = ''
        const cards = arr.map(card => this._createCard(card))
        this.container.append(...cards)
    }

    renderModal(data){
        this.modal.classList.add('active')
        this.modal.innerHTML = ` <div class="modal__container">
        <div class="pfotoModal">
            <img src="img/${data.imgUrl}" alt="">
        <div class="ModalInfo">
            <div class="blockModal">
                <p class="name">${data.name}</p>
                   <div class="CardsModal">
                      <div class="left">
                          <img src="img/icons/like_empty.svg" alt="">
                        </div>
                        <div class="center">
                            <p>${data.orderInfo.reviews} Positive reviews</p>
                            <p>Adove avarage</p>
                        </div>
                        <div class="right">
                            <p>${Math.floor(Math.random() * (10000 - 0) + 0)} orders</p>
                        </div>
                    </div>
                    <div class="paragraf">
                        <p>Color: ${data.color.map(item => `<li>${item}</li>`).join('')}
                        </p>
                        <p>Operating System: ${data.os}</p>
                        <p>Chip: ${data.chip.name}</p>
                        <p>Heigth: ${data.size.height}</p>
                        <p>Width: ${data.size.width}</p>
                        <p>Depth: ${data.size.depth}</p>
                        <p>Weight: ${data.size.weight}</p>
                    </div>
                </div>
            </div>
        <div class="modalPrice">
            <div class="center">
                <h2>${data.price} $</h2>
                <p>Stock: ${data.orderInfo.inStock}.</p>
                <div class="button">
                    <button class="button">add to cart</button>
                </div>
            </div>
        </div>    
    </div>
        `
        this.modal.onclick = () => {
            if (this.modal !== event.target) return;
            this.modal.classList.remove('active')
        }
    }

    // renderColor(data){
    //     const getColorMap = () => {
    //         return items.reduce((acc, item) => {
    //             return {
    //                 ...acc,
    //                 [item.color]: item.color in acc ? [...acc[item.color], item] : [item]
    //             }
    //         }, {})
    //     }
    // }
    
}

const card = new Card()



$(document).ready(function(){
	$('.slider').slick({
		arrows:true,
		dots:true,
		slidesToShow:1,
		autoplay:true,
		speed:2000,
		autoplaySpeed:2000,
		responsive:[
			{
				breakpoint: 768,
				settings: {
					slidesToShow:2
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow:1
				}
			}
		]
	});
});